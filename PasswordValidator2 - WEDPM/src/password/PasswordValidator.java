package password;

public class PasswordValidator {

	private static int MIN_LENGTH = 8;

	
	/**
	 * 
	 * @param password
	 * @return true if number of characters is 8 or more. No spaces allowed
	 * Opted for a single return rather than 2 separate returns
	 */
	public static boolean isValidLength(String password) {
		
		return password.indexOf( " " ) < 0 && password.length() >= MIN_LENGTH;
		
		/*if(password.indexOf(" ") >= 0) {
			return false;
		}
		
		return password.length() >= MIN_LENGTH;*/
		

	}
	
	public static boolean containsTwoDigits(String password) {
		int counter = 0;
		
		for(char c : password.toCharArray()) {
			if (Character.isDigit(c)) {
				counter++;
			}
		}
		
		return (counter >= 2);
	}
	
	public static boolean hasValidCaseChars(String password) {
		return password != null && password.matches("^.*[a-z].*$") && password.matches ("^.*[A-Z].*$");
	}
	
}
